package org.posa;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AddressBookTest {

    //Avem nevoie sa curatam agenda inainte de fiecare test in parte
    @BeforeEach
    public void asd() {
        AddressBook.memory.clear();
    }

    @Test
    public void when2PersonsAreAddedThen2PersonsAreRetrieved() {
        AddressBook.add(new Person("Bogdan", "123"));
        AddressBook.add(new Person("Andrei", "2323"));
        assertEquals(2, AddressBook.getAll().size());
    }

    @Test
    public void when3PersonsAreAddedThen3PersonsAreRetrieved() {
        AddressBook.add(new Person("Bogdan", "123"));
        AddressBook.add(new Person("Andrei", "2323"));
        AddressBook.add(new Person("Maria", "555"));
        assertEquals(3, AddressBook.getAll().size());
    }
}
