package org.posa;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class SetUtilsTest {

    @Test
    public void whenBothSetsAreEmpty_thenTheResultIsNull() {
        SetUtils setUtils = new SetUtils();
        Set set1 = new HashSet();
        Set set2 = new HashSet();

        Set result = setUtils.setDifference(set1, set2);
        assertNull(result);
    }

    @Test
    public void whenFirstSetIsEmptyAndTheSecondSetIsNotEmpty_thenTheResultIsNull() {
        SetUtils setUtils = new SetUtils();
        Set set1 = new HashSet();
        Set set2 = new HashSet();
        set2.add(4);
        set2.add(5);
        Set result = setUtils.setDifference(set1, set2);
        assertNull(result);
    }

    @Test
    public void whenBothSetsAreNotEmpty_thenTheResultIsNotEmpty() {
        SetUtils setUtils = new SetUtils();

        Set set1 = new HashSet();
        set1.add(10);

        Set set2 = new HashSet();
        set2.add(4);
        set2.add(5);

        Set result = setUtils.setDifference(set1, set2);
        assertTrue(!result.isEmpty());
    }

    @Test
    public void whenFirstSetIsNotEmptyAndSecondSetIsEmpty_thenTheResultIsNotEmpty() {
        SetUtils setUtils = new SetUtils();

        Set set1 = new HashSet();
        set1.add(10);

        Set set2 = new HashSet();

        Set result = setUtils.setDifference(set1, set2);
        assertTrue(!result.isEmpty());
    }

    @Test
    public void whenFirstSetHaveSomeIdenticalValuesWithTheSecondSet_thenTheResultIsCorrect() {
        SetUtils setUtils = new SetUtils();

        Set set1 = new HashSet();
        set1.add(10);
        set1.add(30);

        Set set2 = new HashSet();
        set2.add(10);
        set2.add(20);
        Set result = setUtils.setDifference(set1, set2);
        assertEquals(1, result.size());
        assertEquals(30, result.iterator().next());
    }

    @Test
    public void whenFirstSetDoesNotHaveSomeIdenticalValuesWithTheSecondSet_thenTheResultIsFirstSet() {
        SetUtils setUtils = new SetUtils();

        Set set1 = new HashSet();
        set1.add(10);
        set1.add(30);

        Set set2 = new HashSet();
        set2.add(50);
        set2.add(20);
        Set result = setUtils.setDifference(set1, set2);
        assertEquals(result, Set.of(10,30));
    }

    @Test
    public void whenFirstSetIsNull_thenAnExceptionIsThrown() {
        SetUtils setUtils = new SetUtils();
        Set set2 = new HashSet();
        set2.add(50);
        set2.add(20);

        assertThrows(NullPointerException.class,
                () -> setUtils.setDifference(null, set2));
    }
}
