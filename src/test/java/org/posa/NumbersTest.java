package org.posa;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class NumbersTest {

    //black-box testing
    //C="relatia fata de 0" -> (-inf, 0), 0, (0, inf)

    @ParameterizedTest
    @CsvSource({"2,positive", "-78,negative", "0,zero"})
    public void test(int inputNumber, String expectedResult) {
        String actualResult = Numbers.calculateSign(inputNumber);
        assertEquals(actualResult, expectedResult);
    }

}