package org.posa;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class YearsTest {

    @ParameterizedTest
    @CsvSource({"2004,true", "2005,false", "1900,false", "2000, true"})
    public void test(int inputYear, boolean expectedResult) {
        boolean actualResult = Years.isLeapYear(inputYear);
        assertEquals(actualResult, expectedResult);
    }

    @RepeatedTest(5)
    public void test() {
        System.out.println("ceva");
    }

}