package org.posa;

public class Numbers {

    /**
     * Calculates the sign of a number
     * @param a the integer for which the sign is calculated
     * @return a String representing the sign of the parameter a
     */
    public static String calculateSign(int a) {
        if (a == 0) {
            return "zero";
        }
        if (a < 0) {
            return "negative";
        }
        return "positive";
    }
}
