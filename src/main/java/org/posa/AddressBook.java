package org.posa;

import java.util.ArrayList;
import java.util.List;

public class AddressBook {
    static List<Person> memory = new ArrayList<>();

    public static void add(Person person) {
        memory.add(person);
    }

    public static List<Person> getAll() {
        return memory;
    }
}

class Person {
    String name;
    String phone;

    public Person(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }
}
