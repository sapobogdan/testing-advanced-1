package org.posa;

import java.util.HashSet;
import java.util.Set;

public class SetUtils {

    //Characteristic

    //----1----
    // C1: Setul set1 este gol: (este gol), (nu este gol)
    // C2: Setul set2 este gol: (este gol), (nu este gol)
    //Test 1 - (gol, gol)
    //Test 2 - (gol, nu este gol)
    //Test 3 - (nu este gol, nu este gol)
    //Test 4 - (nu este gol, este gol)

    //----2----
    //C: Set 1 are ceva in comun cu set 2: (da), (nu)
    //Test 1 - set 1 are ceva in comun cu setul 2 ->
    //Test 2 - set 1 nu are nimic in comun cu setul 2 ->
    public Set setDifference(Set set1, Set set2) {
        if (set1 == null || set2 == null) {
            throw new NullPointerException();
        }

        Set result = new HashSet(set1);
        result.removeAll(set2);

        if (result.isEmpty()) {
            return null;
        }

        return result;
    }
}
